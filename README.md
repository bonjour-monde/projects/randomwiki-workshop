**R A N D O M - W I K I**<br>
: :  La Martinière - Diderot<br>
: :  31 janvier - 3 février 2023<br>

————————————<br>
Comment trouver de l'inspiration en *type design* si l'on met de côté les angles historiques et techniques ? Nous aurons recourt à une technique quelque peu radicale : celle d'un corpus d'informations pris au hasard, sous la forme d'un article Wikipédia sélectionné via la fonction *Random Article* de la plateforme en ligne. à partir de là, il s'agira pour chaun.e de mener un travail d'enquête, en passant cet article au peigne afin afin d'y déceler des éléments qui en permettront la traduction en formes typographiques.<br>
Le travail de dessin ne se fera à partir de rien, mais commencera par la manipulation du caractère modulaire Capela, qu'il faudra progressivement s'approprier. Cette méthode permettra la compréhension de l'aspect fortement modulaire de notre système d'écriture.<br>
Les caractères typographiques seront ensuite mis à disposition via un site spécimen dédié, pour lequel chacun.e devra produire des visuels, mettant en scène les formes produites et leur thématique d'origine.<br> 


————————————<br>
**C A L E N D R I E R**<br><br>
• **Mardi 31 janvier**<br>
    - Lancement/introduction du sujet<br>
    - Sélection des articles d'inspiration<br>
    - Collecte d'images et de mots<br>
    -- Pause déjeuner<br>
    - Recherche de rythmes graphiques et calligraphiques<br>
    - Introduction au logiciel Glyphs et à la notion de composantes<br>
    <br>
• **Mercredi 1 février**<br>
    - Travail de dessin sur Glyphs<br>
    - Suivi individuel<br>
    -- Pause déjeuner<br>
    - Présentation du travail de Lucas Descroix<br>
    - Travail de dessin sur Glyphs<br>
    - Suivi individuel<br>
    <br>
• **Jeudi 2 février**<br>
    - Poursuite du travail sur Glyphs (en autonomie)<br>
    -- Pause déjeuner<br>
    - Point technique sur le *spacing*<br>
    - Poursuite du travail sur Glyphs avec focus sur le *spacing*<br>
    - Suivi individuel<br>
    <br>
• **Vendredi 3 février**<br>
    - Fin du travail sur Glyphs<br>
    - Exports des fichiers typographiques<br>
    -- Pause déjeuner<br>
    - Mise en page de visuels ”spécimen” (ca. 5 par caractère, noir&blanc, 1200x1200px, PNG)<br>
    - Intégration des visuels dans la page web<br>
    - Restitution en commun<br>



————————————<br>
**L I E N S**

• [Télécharger le logiciel Glyphs](https://glyphsapp.com/buy)<br>
• [Ressources typographiques diverses (rassemblées par Studio Triple)](https://gitlab.com/StudioTriple/vite_et_mal/-/blob/master/documentation/type%20design%20education%20ressources.md)<br>
• [Tutoriels pour le logiciel Glyphs](https://glyphsapp.com/learn)<br>
• [Oh No Type Co. blog](https://ohnotype.co/blog/tagged/teaching)<br>
• [Type Review Journal](https://fontreviewjournal.com/)<br>
• [Fonts In Use](https://fontsinuse.com/)<br>
• [Guide pour le spacing, Fontsmith](https://www.fontsmith.com/blog/2018/02/05/how-to-space-a-typeface)<br>
• [Guides pour le spacing, Society of Fonts](https://www.societyoffonts.com/2018/09/26/spacing-a-font-part-2/)<br>
• [Article sur le spacing, OHno](https://ohnotype.co/blog/spacing)<br>

————————————<br>
**R É F É R E N C E S**

MODULES<br>
• [MODULES > Capitales (Edward Catich, ‘The Origin of the Serif’)](https://pampatype.com/thumbs/blog/reforma/trazos-pincel-catich-1200x536.png)<br>
• [MODULES > Capitales (Corentin Noyer, ‘Manipulation Typographique’)](https://anrt-nancy.fr/anrt-22/media/pages/projets/manipulation-typographique/b5232b4de5-1650381072/anrt-2015-2016-noyer-03.jpg)<br>
• [MODULES > bas-de-casse (Studio Triple, ‘Modules et anatomie’)](https://gitlab.com/StudioTriple/vite_et_mal/-/blob/master/documentation/01%20modules%20et%20anatomie.pdf)<br><br>
FONDERIES / SPÉCIMENS WEB<br>
• [Spécimens typographiques web](https://typespecimens.xyz/specimens/all/)<br>
• [Commercial Classics Showcase](https://showcase.commercialclassics.com/)<br>
• [OHno Type Co.](https://ohnotype.co/fonts)<br>
• [General Type Studio](https://www.generaltypestudio.com/)<br>
• [The Pyte Foundry](https://thepytefoundry.net/)<br><br>
CARACTÈRES<br>
• [Electric](https://www.myfonts.com/fonts/typodermic/electric/?refby=fiu)<br>
• [Elektrix](https://www.emigre.com/Fonts/Elektrix)<br>
• [Gemini](https://www.flickr.com/photos/stewf/16437312062/)<br>
• [GlyphWorld](https://femme-type.com/a-typeface-of-nine-landscapes-glyph-world/)<br>
• [Grassy](https://www.fontshop.com/families/linotype-grassy)<br>
• [Kaeru Kaeru](http://velvetyne.fr/fonts/kaeru-kaeru/)<br>
• [Pilowlava](http://velvetyne.fr/fonts/pilowlava/)<br>
• [Sea Weed](https://www.flickr.com/photos/hardwig/46965181465/in/photostream/)<br>
• [Wind](https://www.typotheque.com/fonts/wind)<br>
